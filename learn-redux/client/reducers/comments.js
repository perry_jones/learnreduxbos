
// handles the entire comment state
function comments(state=[], action){

    if (typeof action.postId !== 'undefined'){
        return {
            // take the current state
            ...state,

            //overwrite this post with the new one
            [action.postId]: postComments(state[action.postId], action)
        }
    }

    return state;

}


function postComments(state = [], action ){
    switch(action.type){
        case 'ADD_COMMENT':
            // return the new state with the new comment
            return [...state, {
                user:action.author,
                text: action.comment
            }];

        case 'REMOVE_COMMENT':
            console.log(action);
            return [
                // everything before the comment
                ...state.slice(0, action.i),
                // everything after the comment
                ...state.slice(action.i+1)
            ];

        default:
            return state;

    }
    return state;
}

export default comments;