import React from 'react';
import Photo from './Photo.js';
import Comments from './Comments.js';

export default class Single extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        // get the index of the post from the URL id
        
        const {postId} = this.props.params;
        const i = this.props.posts.findIndex((post) => post.code === postId);

        // get us the post
        const post = this.props.posts[i];

        const postComments = this.props.comments[postId] || [];

        return (
            <div className="single-photo">
                <Photo post={post} i={i} {...this.props}/>
                <Comments postComments={postComments} {...this.props} />
            </div>
        )
    }
}